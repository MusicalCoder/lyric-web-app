/* example song layout
{
	"id": 0
	"file":"",
	"title":"",
	"project":"all"
}
*/
var songList = [
{
	"id": 1,
	"file":"allsummer",
	"title":"All Summer Long",
	"project":"ttg"
},
{
	"id": 2,
	"file":"amergirl",
	"title":"American Girl",
	"project":"ttg"
},
{
	"id": 3,
	"file":"anotherbite",
	"title":"Another 1 Bites",
	"project":"ttg"
},
{
	"id": 4,
	"file":"badgirlfriend",
	"title":"Bad Girlfriend",
	"project":"ttg"
},
{
	"id": 5,
	"file":"billiejean",
	"title":"Billie Jean",
	"project":"ttg"
},
{
	"id": 6,
	"file":"bluecolor",
	"title":"Blue Ain't Your Color",
	"project":"ttg"
},
{
	"id": 7,
	"file":"breakchain",
	"title":"Breaking The Chains",
	"project":"ttg"
},
{
	"id": 8,
	"file":"brickhouse",
	"title":"Brick House",
	"project":"ttg"
},
{
	"id": 9,
	"file":"browneye",
	"title":"Brown Eyed Girl",
	"project":"ttg"
},
{
	"id": 10,
	"file":"cakeocean",
	"title":"Cake By The Ocean",
	"project":"ttg"
},
{
	"id": 11,
	"file":"centerfold",
	"title":"Centerfold",
	"project":"ttg"
},
{
	"id": 12,
	"file":"dontstop",
	"title":"Don't Stop Believin'",
	"project":"ttg"
},
{
	"id": 13,
	"file":"doublevision",
	"title":"Double Vision",
	"project":"ttg"
},
{
	"id": 14,
	"file":"entersand",
	"title":"Enter Sandman",
	"project":"ttg"
},
{
	"id": 15,
	"file":"everybody",
	"title":"Everybody Wants You",
	"project":"ttg"
},
{
	"id": 16,
	"file":"faith",
	"title":"Faith",
	"project":"ttg"
},
{
	"id": 17,
	"file":"fatbottom",
	"title":"Fat Bottom Girls",
	"project":"ttg"
},
{
	"id": 18,
	"file":"feelmakelove",
	"title":"Feel Like Makin' Love",
	"project":"ttg"
},
{
	"id": 19,
	"file":"fightright",
	"title":"Fight For Your Right",
	"project":"ttg"
},
{
	"id": 20,
	"file":"footloose",
	"title":"Footloose",
	"project":"ttg"
},
{
	"id": 21,
	"file":"gimme3steps",
	"title":"Gimme 3 Steps",
	"project":"ttg"
},
{
	"id": 22,
	"file":"getoverit",
	"title":"Get Over It",
	"project":"ttg"
},
{
	"id": 23,
	"file":"sloopy",
	"title":"Hang On Sloopy",
	"project":"ttg"
},
{
	"id": 24,
	"file":"harder2breathe",
	"title":"Harder To Breathe",
	"project":"all"
},
{
	"id": 25,
	"file":"highground",
	"title":"Higher Ground",
	"project":"ttg"
},
{
	"id": 26,
	"file":"honkytonkwoman",
	"title":"Honkytonk Woman",
	"project":"ttg"
},
{
	"id": 27,
	"file":"holdmyhand",
	"title":"Hold My Hand",
	"project":"mbp"
},
{
	"id": 28,
	"file":"hungrywolf",
	"title":"Hungry Like The Wolf",
	"project":"ttg"
},
{
	"id": 29,
	"file":"hurtgood",
	"title":"Hurt So Good",
	"project":"ttg"
},
{
	"id": 30,
	"file":"fallinlove",
	"title":"I'll Fall In Love Again",
	"project":"ttg"
},
{
	"id": 31,
	"file":"inmydreams",
	"title":"In My Dreams",
	"project":"ttg"
},
{
	"id": 32,
	"file":"nodoctor",
	"title":"No Doctor",
	"project":"ttg"
},
{
	"id": 33,
	"file":"jessiegirl",
	"title":"Jessie's Girl",
	"project":"ttg"
},
{
	"id": 34,
	"file":"keephands",
	"title":"Hands To Yourself",
	"project":"ttg"
},
{
	"id": 35,
	"file":"learn2fly",
	"title":"Learn To Fly",
	"project":"ttg"
},
{
	"id": 36,
	"file":"livmidnight",
	"title":"Living After Midnight",
	"project":"ttg"
},
{
	"id": 37,
	"file":"livprayer",
	"title":"Living On A Prayer",
	"project":"ttg"
},
{
	"id": 38,
	"file":"longtrain",
	"title":"Long Train Running",
	"project":"ttg"
},
{
	"id": 39,
	"file":"loveshack",
	"title":"Love Shack",
	"project":"all"
},
{
	"id": 40,
	"file":"margaritaville",
	"title":"Margaritaville",
	"project":"ttg"
},
{
	"id": 41,
	"file":"moredrink",
	"title":"More I Drink",
	"project":"ttg"
},
{
	"id": 42,
	"file":"mustangsally",
	"title":"Mustang Sally",
	"project":"ttg"
},
{
	"id": 43,
	"file":"ownworst",
	"title":"My Own Worst Enemy",
	"project":"ttg"
},
{
	"id": 44,
	"file":"oneilove",
	"title":"The One I Love",
	"project":"ttg"
},
{
	"id": 45,
	"file":"funkymusic",
	"title":"Play That Funky Music",
	"project":"ttg"
},
{
	"id": 46,
	"file":"rockusa",
	"title":"R.O.C.K. In The USA",
	"project":"ttg"
},
{
	"id": 47,
	"file":"rockymtnway",
	"title":"Rocky Mountain Way",
	"project":"ttg"
},
{
	"id": 48,
	"file":"sevenbridges",
	"title":"Seven Bridges Road",
	"project":"ttg"
},
{
	"id": 49,
	"file":"shoutoutloud",
	"title":"Shout It Out Loud",
	"project":"ttg"
},
{
	"id": 50,
	"file":"shutupdance",
	"title":"Shut Up And Dance",
	"project":"ttg"
},
{
	"id": 51,
	"file":"wonderful",
	"title":"Some Kind Wonderful",
	"project":"ttg"
},
{
	"id": 52,
	"file":"savehorse",
	"title":"Save A Horse",
	"project":"ttg"
},
{
	"id": 53,
	"file":"sepways",
	"title":"Separate Ways",
	"project":"ttg"
},
{
	"id": 54,
	"file":"stuckmiddle",
	"title":"Stuck In The Middle",
	"project":"ttg"
},
{
	"id": 55,
	"file":"takecarebiz",
	"title":"Taking Care Of Biz",
	"project":"ttg"
},
{
	"id": 56,
	"file":"tenwhiskey",
	"title":"Tennessee Whiskey",
	"project":"ttg"
},
{
	"id": 57,
	"file":"tiemother",
	"title":"Tie Your Mother Down",
	"project":"ttg"
},
{
	"id": 58,
	"file":"turnloose",
	"title":"Turn Me Loose",
	"project":"ttg"
},
{
	"id": 59,
	"file":"turnjap",
	"title":"Turning Japanese",
	"project":"ttg"
},
{
	"id": 60,
	"file":"underbridge",
	"title":"Under The Bridge",
	"project":"ttg"
},
{
	"id": 61,
	"file":"upfunk",
	"title":"Uptown Funk",
	"project":"ttg"
},
{
	"id": 62,
	"file":"warpig",
	"title":"War Pigs",
	"project":"ttg"
},
{
	"id": 63,
	"file":"likeaboutyou",
	"title":"What I Like About You",
	"project":"ttg"
},
{
	"id": 64,
	"file":"wildwest",
	"title":"Wild Wild West",
	"project":"ttg"
},
{
	"id": 65,
	"file":"workweekend",
	"title":"Working / Weekend",
	"project":"ttg"
},
{
	"id": 66,
	"file":"tookthewords",
	"title":"You Took The Words",
	"project":"ttg"
},
{
	"id": 67,
	"file":"lethercry",
	"title":"Let Her Cry",
	"project":"mbp"
},
{
	"id": 68,
	"file":"youandme",
	"title":"You And Me",
	"project":"mbp"
},
{
	"id": 69,
	"file":"thinkoutloud",
	"title":"Thinking Out Loud",
	"project":"mbp"
},
{
	"id": 70,
	"file":"perfect",
	"title":"Perfect",
	"project":"mbp"
},
{
	"id": 71,
	"file":"heartmatter",
	"title":"Heart Of The Matter",
	"project":"mbp"
},
{
	"id": 72,
	"file":"dontthink",
	"title":"I Don't Think I Will",
	"project":"mbp"
},
{
	"id": 73,
	"file":"everyrose",
	"title":"Every Rose Has Its Thorn",
	"project":"mbp"
},
{
	"id": 74,
	"file":"wonderfultonite",
	"title":"Wonderful Tonight",
	"project":"mbp"
},
{
	"id": 75,
	"file":"takeiteasy",
	"title":"Take It Easy",
	"project":"mbp"
},
{
	"id": 76,
	"file":"sweethomebama",
	"title":"Sweet Home Alabama",
	"project":"mbp"
},
{
	"id": 77,
	"file":"wemakelove",
	"title":"When We Make Love",
	"project":"mbp"
},
{
	"id": 78,
	"file":"hotelcalifornia",
	"title":"Hotel California",
	"project":"mbp"
},
{
	"id": 79,
	"file":"inyoureyes",
	"title":"In Your Eyes",
	"project":"mbp"
},
{
	"id": 80,
	"file":"anotherthing",
	"title":"Another Thing Comin",
	"project":"ttg"
},
{
	"id": 81,
	"file":"folsomprison",
	"title":"Folsom Prison Blues",
	"project":"ttg"
},
{
	"id": 82,
	"file":"youmyhome",
	"title":"You're My Home",
	"project":"mbp"
},
{
	"id": 83,
	"file":"blurredlines",
	"title":"Blurred Lines",
	"project":"ttg"
},
{
	"id": 84,
	"file":"beerdrinkers",
	"title":"Beer Drinkers",
	"project":"all"
},
{
	"id": 85,
	"file":"driveinc",
	"title": "Drive",
	"project":"ttg"
},
{
	"id": 86,
	"file":"madeloveyou",
	"title": "Made 4 Loving U",
	"project": "ttg"
},
{
	"id": 87,
	"file": "whipit",
	"title": "Whip It",
	"project": "ttg"
},
{
	"id": 88,
	"file": "beautifulcrazy",
	"title": "Beautiful Crazy",
	"project": "ttg"
},
{
	"id": 89,
	"file": "twilightzone",
	"title": "Twilight Zone",
	"project": "ttg"
},
{
	"id": 90,
	"file": "acdc-tnt",
	"title": "T.N.T.",
	"project": "ttg"
},
{
	"id": 91,
	"file": "roadhouse",
	"title": "Road House Blues",
	"project": "ttg"
},
{
	"id": 92,
	"file": "immigrant",
	"title": "Immigrant Song",
	"project": "ttg"
},
{
	"id": 93,
	"file": "breaklaw",
	"title": "Breaking The Law",
	"project": "ttg"
},
{
	"id": 94,
	"file": "bluecollar",
	"title": "Blue Collar Man",
	"project": "ttg"
},
{
	"id": 95,
	"file": "8675309",
	"title": "867-5309 (Jenny)",
	"project": "ttg"
},
{
	"id": 96,
	"file": "ready2go",
	"title": "Ready To Go",
	"project": "ttg"
},
{
	"id": 97,
	"file": "superfly",
	"title": "Super Fly",
	"project": "ttg"
},
{
	"id": 98,
	"file": "hereparty",
	"title": "Here for the Party",
	"project": "ttg"
},
{
	"id": 99,
	"file": "justagirl",
	"title": "Just A Girl",
	"project": "ttg"
},
{
	"id": 100,
	"file": "rowdy",
	"title": "Rowdy",
	"project": "ttg"
},
{
	"id": 101,
	"file": "kerosene",
	"title": "Kerosene",
	"project": "ttg"
},
{
	"id": 102,
	"file": "sowhat",
	"title": "So What",
	"project": "ttg"
},
{
	"id": 103,
	"file": "celebskin",
	"title": "Celebrity Skin",
	"project": "ttg"
},
{
	"id": 104,
	"file": "fastestgirl",
	"title": "Fastest Girl / Town",
	"project": "ttg"
},
{
	"id": 105,
	"file": "exandoh",
	"title": "Ex's & Oh's",
	"project": "ttg"
},
{
	"id": 106,
	"file": "mamabreak",
	"title": "Mama's Broken Heart",
	"project": "ttg"
},
{
	"id": 107,
	"file": "hatemyself",
	"title": "Hate Myself...",
	"project": "ttg"
},
{
	"id": 108,
	"file": "monymony",
	"title": "Mony Mony",
	"project": "ttg"
},
{
	"id": 109,
	"file": "holdonloosely",
	"title": "Hold On Loosely",
	"project": "ttg"
},
{
	"id": 110,
	"file": "shattered",
	"title": "Shattered",
	"project": "ttg"
},
{
	"id": 111,
	"file": "basketcase",
	"title": "Basket Case",
	"project": "ttg"
},
{
	"id": 112,
	"file": "wordup",
	"title": "Word Up",
	"project": "ttg"
},
{
	"id": 113,
	"file": "dontchange",
	"title": "Don't Change",
	"project": "ttg"
},
{
	"id": 114,
	"file": "heavy",
	"title": "Heavy",
	"project": "ttg"
},
{
	"id": 115,
	"file": "freeworld",
	"title": "Rockin' Free World",
	"project": "ttg"
},
{
	"id": 116,
	"file": "godblessusa",
	"title": "God Bless USA",
	"project": "ttg"
},
{
	"id": 117,
	"file": "comegetlove",
	"title": "Come & Get Ur Love",
	"project": "ttg"
},
{
	"id": 118,
	"file": "shadowdance",
	"title": "Shadow Dancing",
	"project": "ttg"
},
{
	"id": 119,
	"file": "shakin",
	"title": "Shakin'",
	"project": "ttg"
},
{
	"id": 120,
	"file": "shame_shame",
	"title": "Shame - Shame",
	"project": "ttg"
},
{
	"id": 121,
	"file": "bitch",
	"title": "Bitch",
	"project": "ttg"
},
{
	"id": 122,
	"file": "drunkgohome",
	"title": "Drunk Go Home",
	"project": "ttg"
},
{
	"id": 123,
	"file": "7nation",
	"title": "7 Nation Army",
	"project": "ttg"
},
{
	"id": 124,
	"file": "aboutdamntime",
	"title": "About Damn Time",
	"project": "ttg"
},
{
	"id": 125,
	"file": "yourlove",
	"title": "Your Love",
	"project": "ttg"
},
{
	"id": 126,
	"file": "barracuda",
	"title": "Barracuda",
	"project": "ttg"
},
{
	"id": 127,
	"file": "youshookme",
	"title": "You Shook Me",
	"project": "ttg"
},
{
	"id": 128,
	"file": "thunderstruck",
	"title": "Thunderstruck",
	"project": "ttg"
},
{
	"id": 129,
	"file": "sirius",
	"title": "Sirius",
	"project": "ttg"
},
{
	"id": 130,
	"file": "zombie",
	"title": "Zombie",
	"project": "ttg"
},
{
	"id": 131,
	"file": "sonsinner",
	"title": "Son of a Sinner",
	"project": "ttg"
},
{
	"id": 132,
	"file": "heaven_llb",
	"title": "Heaven",
	"project": "ttg"
},
{
	"id": 133,
	"file": "dontstartnow",
	"title": "Don't Start Now",
	"project": "ttg"
},
{
	"id": 134,
	"file": "linger",
	"title": "Linger",
	"project": "ttg"
},
{
	"id": 135,
	"file": "getlucky",
	"title": "Get Lucky",
	"project": "ttg"
},
{
	"id": 136,
	"file": "badromance",
	"title": "Bad Romance",
	"project": "ttg"
},
{
	"id": 137,
	"file": "miserybusiness",
	"title": "Misery Business",
	"project": "ttg"
},
{
	"id": 138,
	"file": "raiseglass",
	"title": "Raise Your Glass",
	"project": "ttg"
},
{
	"id": 139,
	"file": "whoknew",
	"title": "Who Knew",
	"project": "ttg"
},
{
	"id": 140,
	"file": "whatsup",
	"title": "What's Up",
	"project": "ttg"
},
{
	"id": 141,
	"file": "partyusa",
	"title": "Party In The USA",
	"project": "ttg"
},
{
	"id": 142,
	"file": "tipsy",
	"title": "A Bar Song",
	"project": "ttg"
},
{
	"id": 143,
	"file": "Flowers",
	"title": "Flowers",
	"project": "ttg"
},
{
	"id": 144,
	"file": "highenough",
	"title": "High Enough",
	"project": "ttg"
},
{
	"id": 145,
	"file": "burndownhouse",
	"title": "Burnin Down House",
	"project": "ttg"
},
{
	"id": 146,
	"file": "pleasechristmas",
	"title": "Please Come Home XMas",
	"project": "mbp"
},
{
	"id": 147,
	"file": "anymore",
	"title": "Anymore",
	"project": "mbp"
},
{
	"id": 148,
	"file": "goodtimes",
	"title": "Good Times",
	"project": "ttg"
},
{
	"id": 149,
	"file": "notgonnatake",
	"title": "We're Not Gonna...",
	"project": "ttg"
}
]
songList.sort(dynamicSort("title"));
