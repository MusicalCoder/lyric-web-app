$home = sessionStorage.getItem("homePage")
$next = sessionStorage.getItem("nextSong")
$list = sessionStorage.getItem("list")
var songList = songList || [];
var setLists = setLists || [];

$headHtml = '<div class="col-2">' +
    `<br/><button type="button" id="btn01" class="btn btn-primary size" onclick="window.location.href='./${$home}'">HOME</button>` +
    '</div>' +
	'<div class="col-2"><br />' +
	`<button type="button" id="btnnexttop" class="btn btn-primary size" onclick="LoadPage(${$next},'${$list}')">NEXT</button></div>` +
    '<div class="col-8">' +
    '<br/><div class="songTitle" id="songTitle"></div>' +
    '</div>';

$footHtml = '<div class="col-2">' +
	`<button type="button" id="btn02" class="btn btn-primary size" onclick="window.location.href='./${$home}'">HOME</button>` +
	'</div>' +
	'<div class="col-2">' +
	`<button type="button" id="btnnextbot" class="btn btn-primary size" onclick="LoadPage(${$next},'${$list}')">NEXT</button>` +
	'</div><br /><br /><hr>';
	
function LoadSong() {
    $('#headerRow').html($headHtml);
    $('#songTitle').html(sessionStorage.getItem("songTitle"));
	$('#footerRow').html($footHtml)
	if ($next === 'xx') {
		$('#btnnexttop').hide();
		$('#btnnextbot').hide();
	}
}

function BuildButtons(page) {
	return $html;
}


function LoadButtons(page) {
	$html = '';
	bySets = (page.substr(0,3) === "set")
	counter = 0;
	maxCounter = bySets ? 2 : 5

	if (bySets) songList.sort(dynamicSort(page))

	songList.forEach(function (entry) {
		if (counter === maxCounter) {
			$html += "<br/><br/>";
			counter = 0;
		}
		
		if (bySets) {
			if (entry[page] !== "") {
				$html += `&nbsp;<button type="button" class="btn btn-primary size" onclick="LoadPage(${entry.id})">${entry.title}</button>&nbsp;`;
				counter++;
			}
		} else {
			if (entry.project === page || entry.project === "all") {
				$html += `&nbsp;<button type="button" class="btn btn-primary size" onclick="LoadPage(${entry.id})">${entry.title}</button>&nbsp;`;
				counter++;
			}
		}
	});

	$('#placeButtons').html($html);
}

function LoadPage(id, set='xx') {
	song = getSong(id);
	if (set !== 'xx') {
		nxt = getNextID(id, set)
		sessionStorage.setItem("nextSong", nxt);
		sessionStorage.setItem("list", set)
	}
	sessionStorage.setItem("songTitle", song.title);
	window.location.href = `./${song.file}.html`;
}

function LoadSetButtons(set) {
	$html = ''

	counter = 0;

	songs = setLists[set]

	songs.forEach(function (id) {
		song = getSong(id);
		if (!song.isEmpty()) {
			$html += `&nbsp;<button type="button" class="btn btn-primary size" onclick="LoadPage(${song.id},'${set}')">${song.title}</button>&nbsp;`;
			counter++;
			if (counter % 4 === 0) {
				$html += "<br/><br/>";
				counter = 0;
			}
		}
	});

	$('#placeButtons').html($html);
}